--
-- Structure de la table `liste_proces`
--

CREATE TABLE IF NOT EXISTS `liste_proces` (
  `id` int(11) NOT NULL auto_increment,
  `titre` text NOT NULL,
  `auteur` int(11) NOT NULL,
  `fichier` text NOT NULL,
  `jouable` int(11) NOT NULL,
  `collaborateurs` text NOT NULL,
  `testeurs` text NOT NULL,
  `langue` varchar(2) NOT NULL,
  `star` smallint(6) NOT NULL default '0',
  `groupe` text NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 ;
