/*
Ace Attorney Online - Wrappers for objects and arrays.

These functions return different kinds of "wrappers" for objects.
A wrapper is an object that behaves like the original object from an outsider's perspective, but transparently extends it with additional behaviour.
*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'objects_buffer_wrappers',
	dependencies : ['objects'],
	init : function() {}
}));

//INDEPENDENT INSTRUCTIONS
function getObjectExtensionWrapper(originalObject, objectModel)
{
	var extensionWrapper = Object.create(null);
	
	// Go through the properties the original object should have according to model.
	for(var propertyName in objectModel)
	{
		(function(propertyName) {
			// Define them as accessors on the wrapper with custom getters and setters.
			Object.defineProperty(extensionWrapper, propertyName, {
				enumerable: true,
				
				// Get the property or a buffer for it.
				// Comment
				get: function() {
					if(propertyName in originalObject)
					{
						// If the property is present on the original object, get an extension wrapper for it.
						return getExtensionWrapperFor(originalObject[propertyName], objectModel[propertyName]);
					}
					else if(propertyName in objectModel)
					{
						// Else, set property in the original object to a clone of the object model's default value and return it directly.
						originalObject[propertyName] = objClone(objectModel[propertyName]);
						return originalObject[propertyName];
					}
					else
					{
						// Else, value is null...
						return null;
					}
				},
				
				// Set the property.
				set: function(new_value) {
					// Update the original object.
					originalObject[propertyName] = new_value;
				}
			});
		})(propertyName);
	}
	
	// Prevent further extensions : properties not present in the object model are not handled.
	Object.preventExtensions(extensionWrapper);
	
	return extensionWrapper;
}

function getArrayExtensionWrapper(originalArray, rowObjectModel)
{
	var extensionWrapper = Object.create(null);
	
	// Set the accessors for a given index.
	function setExtensionAccessorForIndex(index)
	{
		Object.defineProperty(extensionWrapper, index, {
			enumerable: true,
			
			// Get an extension wrapper for the row.
			get: function() {
				if(index < originalArray.length)
				{
					return getExtensionWrapperFor(originalArray[index], rowObjectModel);
				}
				else
				{
					return null;
				}
			},
			
			// Set the row.
			set: function(new_row){
				// Update the original array.
				originalArray[index] = new_row;
			}
		});
	}
	
	// Array length mirrors the original array's.
	var array_length = originalArray.length;
	
	// Iterate through all rows of the extension.
	for(var index = 0; index < array_length; index++)
	{
		// Define them as accessors on the extension with custom getters and setters
		setExtensionAccessorForIndex(index);
	}
	
	// Expose extension's length as an accessor
	Object.defineProperty(extensionWrapper, 'length', {
		get: function() {
			return array_length;
		}
	});
	
	// Implement array push method on the extension
	Object.defineProperty(extensionWrapper, 'push', {
		value: function() {
			originalArray.push.apply(originalArray, arguments);
			
			// Go through all new rows to create accessors.
			for(var i = array_length; i < originalArray.length; i++)
			{
				// Set accessors for this index.
				setExtensionAccessorForIndex(i);
			}
			
			// Update array length to keep mirroring.
			array_length = originalArray.length;
		}
	});
	
	// Implement array push method on the extension
	Object.defineProperty(extensionWrapper, 'splice', {
		value: function() {
			originalArray.splice.apply(originalArray, arguments);
			
			// Go through all new rows to create accessors.
			for(var i = array_length; i < originalArray.length; i++)
			{
				// Set accessors for this index.
				setExtensionAccessorForIndex(i);
			}
			
			// Update array length to keep mirroring.
			array_length = originalArray.length;
		}
	});
	
	// Make extension's JSON representation an array
	Object.defineProperty(extensionWrapper, 'toJSON', {value: function(){
		var realArray = [];
		for(var i = 0; i < array_length; i++)
		{
			realArray[i] = this[i];
		}
		return realArray;
	}});
	
	return extensionWrapper;
}

function getObjectBufferWrapper(originalObject, diffObject)
{
	function activate(diff)
	{
		if('__activate' in diff)
		{
			diff['__activate']();
		}
	}
	
	var buffer = Object.create(null);
	
	// Use provided diff object to apply changes to the buffer wrapper, or use empty diff at first.
	var diff = diffObject || {};
	
	// Define the cache of properties, used at get time. Contains a buffer wrapper if reading from an unedited object property, or the actual new value.
	var cache = {};
	
	// Go through the properties of the original object.
	for(var propertyName in originalObject)
	{
		(function(propertyName) {
			// Define them as accessors on the buffer with custom getters and setters
			Object.defineProperty(buffer, propertyName, {
				enumerable: true,
				
				// Get the property or a buffer for it.
				get: function() {
					if(propertyName in cache)
					{
						// If a value has already been cached for this property, retrieve it.
						return cache[propertyName];
					}
					else if((propertyName in diff) &&
						((diff[propertyName] == null) || 
							(typeof diff[propertyName] != 'object')))
					{
						// If a scalar or null value is in the diff, cache and return it.
						cache[propertyName] = diff[propertyName];
						return cache[propertyName];
					}
					else
					{
						// Prepare the child diff.
						var childDiff;
						if(propertyName in diff)
						{
							// Use diff for this field if any.
							childDiff = diff[propertyName];
						}
						else
						{
							// Otherwise, create a child diff with an __activate method that will insert it in parent diff when needed.
							childDiff = Object.create(null);
							Object.defineProperty(childDiff, '__activate', {value: function() {
								diff[propertyName] = childDiff;
								activate(diff);
							}});
						}
						
						cache[propertyName] = getBufferWrapperFor(originalObject[propertyName], childDiff);
						
						return cache[propertyName];
					}
				},
				
				// Set the property.
				set: function(new_value) {
					// Update the diff and the cache
					diff[propertyName] = (cache[propertyName] = new_value);
					activate(diff);
				}
			});
		})(propertyName);
	}
	
	// Expose buffer's diff and cache
	Object.defineProperty(buffer, '__diff', {value: diff});
	Object.defineProperty(buffer, '__cache', {value: cache});
	
	// Prevent further extensions : properties not present in the original object are not handled.
	Object.preventExtensions(buffer);
	
	return buffer;
}


function getArrayBufferWrapper(originalArray, diffObject)
{
	// Trigger diff activation if it's needed.
	function activate(diff)
	{
		if('__activate' in diff)
		{
			diff['__activate']();
		}
	}
	
	// Check if index is included in a block edit from the diff.
	function getDiffSourceBlockForIndex(index)
	{
		for(var i = 0; i < diff.blockEdits.length; i++)
		{
			if(diff.blockEdits[i].start_index <= index && diff.blockEdits[i].end_index >= index)
			{
				return diff.blockEdits[i];
			}
		}
		
		// If index is not in any block edit, consider it in a "mock" block with shift 0.
		return {shift: 0};
	}
	
	// Set the accessors for a given index.
	function setBufferAccessorForIndex(index)
	{
		Object.defineProperty(buffer, index, {
			enumerable: true,
			
			// Get the row or a buffer for it.
			get: function() {
				if(index in cache)
				{
					// If this row has already been cached, fetch it.
					return cache[index];
				}
				else
				{
					var diffSourceBlock = getDiffSourceBlockForIndex(index);
					
					if('shift' in diffSourceBlock)
					{
						// If index corresponds to an original row, fetch from the original array at corresponding shift and return buffer wrapper if needed.
						var original_index = index - diffSourceBlock.shift;
						if(original_index >= originalArray.length)
						{
							// But indexes out of original array range mean insconsistent diff.
							throw "Inconsistent diff";
						}
						
						var childDiff;
						if(original_index in diff.originalRowEdits)
						{
							// Use diff for this original row if any.
							childDiff = diff.originalRowEdits[original_index];
						}
						else
						{
							// Otherwise, create a child diff with an __activate method that will insert it in parent diff when needed.
							childDiff = Object.create(null);
							Object.defineProperty(childDiff, '__activate', {value: function() {
								diff.originalRowEdits[original_index] = childDiff;
								activate(diff);
							}});
						}
						
						cache[index] = getBufferWrapperFor(originalArray[original_index], childDiff);
					}
					else
					{
						// If index is in a block edit containing inserted rows, fetch directly from these insertions.
						cache[index] = diffSourceBlock.inserts[index - diffSourceBlock.start_index];
					}
					return cache[index];
				}
			},
			
			// Set the row.
			set: function(new_row){
				// Update the diff
				
				var diffSourceBlock = getDiffSourceBlockForIndex(index);
				if(diffSourceBlock.shift)
				{
					// If index from original array, update the diff at original index.
					var original_index = index - diffSourceBlock.shift;
					diff.originalRowEdits[original_index] = new_row;
					activate(diff);
				}
				else
				{
					// If index from inserted block, update the diff source block directly.
					diffSourceBlock.inserts[index - diffSourceBlock.start_index] = new_row;
				}
				
				// Update the cache.
				cache[index] = new_row;
			}
		});
	}
	
	var buffer = Object.create(null);
	
	// Use provided diff object to apply changes to the buffer wrapper, or use empty diff at first.
	var diff = diffObject || {originalRowEdits:{}, blockEdits:[], length: -1};
	
	// Buffer length is read from the diff if possible, else it's the original array's.
	var buffer_length = diff.length >= 0 ? diff.length : originalArray.length;
	
	// Define the cache of rows, used at get time. Contains a buffer wrapper if reading from an unedited object property, or the actual new value.
	var cache = [];
	
	// Iterate through all rows of the buffer.
	for(var index = 0; index < buffer_length; index++)
	{
		// Define them as accessors on the buffer with custom getters and setters
		setBufferAccessorForIndex(index);
	}
	
	// Expose buffer's length as an accessor
	Object.defineProperty(buffer, 'length', {
		get: function() {
			return buffer_length;
		}
	});
	
	// Implement array push method on the buffer
	Object.defineProperty(buffer, 'push', {
		value: function() {
			// Convert arguments into an array of new rows.
			var new_rows = Array.prototype.slice.call(arguments);
			
			// Update the diff
			var lastRowBlock = getDiffSourceBlockForIndex(buffer_length - 1);
			if('inserts' in lastRowBlock)
			{
				// If last row is already in an insert block, push new rows at the end of this block.
				lastRowBlock.inserts.push.apply(lastRowBlock.inserts, new_rows);
				// And update the block end index.
				lastRowBlock.end_index += new_rows.length;
			}
			else
			{
				// If last row is part of the original array, create a new insert block.
				var newLastRowBlock = {
					start_index: buffer_length,
					end_index: buffer_length + new_rows.length - 1,
					inserts: new_rows
				};
				diff.blockEdits.push(newLastRowBlock);
			}
			activate(diff);
			
			// Go through all new rows to cache them and create accessors.
			for(var i = 0; i < new_rows.length; i++)
			{
				// Cache new row at new last index.
				cache[buffer_length] = new_rows[i];
				
				// Set accessors for this index.
				if(!(buffer_length + i in buffer))
				{
					setBufferAccessorForIndex(buffer_length);
				}
				
				// Increment buffer length and update diff.
				diff.length = (++buffer_length);
			}
		}
	});
	
	Object.defineProperty(buffer, 'splice', {
		value: function(op_index, num_deletions) {
			var insertions = Array.prototype.slice.call(arguments, 2);
			var op_shift = insertions.length - num_deletions;
			var deletion_range_end_index = op_index + num_deletions - 1;
			
			// Update diff.
			
			// Go through diff block edits to perform deletion of the requested elements
			var previous_block_end_index = -1;
			var blocksToDelete = [];
			var blocksToInsert = [];
			for(var i = 0; i < diff.blockEdits.length; i++)
			{
				var block = diff.blockEdits[i];
				
				if(op_shift != 0
					&& previous_block_end_index + 1 < block.start_index
					&& op_index < block.start_index
					&& deletion_range_end_index < block.start_index - 1)
				{
					// If there is space between blocks after op index and it isn't entirly in deletion range, insert new shift block.
					blocksToInsert.push({
						start_index: Math.max(op_index, previous_block_end_index + 1) + op_shift,
						end_index: block.start_index - 1 + op_shift,
						shift: op_shift
					});
				}
				previous_block_end_index = block.end_index;
				
				if(op_index <= block.start_index)
				{
					// If block starts after deletion range start...
					
					if(deletion_range_end_index >= block.end_index)
					{
						// ...and ends before deletion range end, delete it completely.
						blocksToDelete.push(i);
					}
					else if(deletion_range_end_index >= block.start_index)
					{
						// ...and starts before deletion range end, beginning of the block is deleted.
						
						var number_of_deleted_elts_in_block = deletion_range_end_index - block.start_index + 1;
						
						if('inserts' in block)
						{
							// If insertion block, then delete the corresponding elements at the beginning.
							block.inserts.splice(0, number_of_deleted_elts_in_block);
						}
						
						// Increase start index by number of deleted elts in the block, then apply shift.
						block.start_index += number_of_deleted_elts_in_block + op_shift;
						block.end_index += op_shift;
					}
					else
					{
						// ...and starts after deletion range end, then just apply the shift.
						block.start_index += op_shift;
						block.end_index += op_shift;
					}
					
					// Also adjust the shift value of shift blocks starting after operation index.
					if('shift' in block)
					{
						block.shift += op_shift;
						
						if(block.shift == 0)
						{
							// If block gets shift 0, it's useless, delete it.
							blocksToDelete.push(i);
						}
					}
				}
				else if(op_index <= block.end_index)
				{
					// If block starts before deletion range start and ends after deletion range start...
					
					if(deletion_range_end_index >= block.end_index)
					{
						// ... and ends before deletion range end, end of the block is contained in deletion range.
						
						if('inserts' in block)
						{
							// Delete corresponding rows from insert block.
							block.inserts.splice(op_index - block.start_index);
						}
						
						// Adjust end index.
						block.end_index = op_index - 1;
					}
					else
					{
						// ... and ends after deletion range end, middle of the block is contained in deletion range.
						
						if('inserts' in block)
						{
							// Delete corresponding rows from insert block and insert new rows inside.
							block.inserts.splice.apply(block.inserts, [op_index - block.start_index, num_deletions].concat(insertions));
							block.end_index += op_shift;
							
							// Insertions performed, forget about them.
							insertions = [];
						}
						else
						{
							// Split shift block in two parts : after and before operation.
							
							// Insert new shift block for elements after op_index.
							blocksToInsert.push({
								start_index: deletion_range_end_index + 1 + op_shift,
								end_index: block.end_index + op_shift,
								shift: op_shift
							});
							
							// Adjust end index : first block ends just before op index.
							block.end_index = op_index - 1;
						}
					}
				}
				// Else, block is entirely before the operation, no action required.
			}
			
			if(op_shift != 0
				&& previous_block_end_index < buffer_length - 1
				&& op_index <= buffer_length - 1
				&& deletion_range_end_index < buffer_length - 1)
			{
				// If non-deleted space after last block, then insert shift block.
				blocksToInsert.push({
					start_index: Math.max(op_index, previous_block_end_index + 1),
					end_index: buffer_length - 1,
					shift: op_shift
				});
			}
			
			if(insertions.length > 0)
			{
				// If insertions required and not performed yet, add a new insert block.
				blocksToInsert.push({
					start_index: op_index,
					end_index: op_index + insertions.length - 1,
					inserts: insertions
				});
			}
			
			// Perform required block deletions.
			for(var i = blocksToDelete.length - 1; i >= 0; i--)
			{
				diff.blockEdits.splice(blocksToDelete[i], 1);
			}
			
			// Perform required block insertions
			for(var i = 0; i < blocksToInsert.length; i++)
			{
				diff.blockEdits.push(blocksToInsert[i]);
			}
			
			// Sort blocks by index order.
			diff.blockEdits.sort(function(a, b){
				return a.start_index - b.start_index;
			});
			
			// TODO : Optimise diff by merging consecutive blocks.
			
			activate(diff);
			
			// Perform splice on cache directly.
			cache.splice.apply(cache, arguments);
			
			// Set accessors for new elts if number of elts increased.
			for(var i = 0; i < op_shift; i++)
			{
				if(!(buffer_length + i in buffer))
				{
					setBufferAccessorForIndex(buffer_length + i);
				}
			}
			
			// Update buffer length.
			buffer_length += op_shift;
			// Update length in diff
			diff.length = buffer_length;
		}
	});
	
	// Expose buffer's diff and cache
	Object.defineProperty(buffer, '__diff', {value: diff});
	Object.defineProperty(buffer, '__cache', {value: cache});
	
	// Make buffer's JSON representation an array
	Object.defineProperty(buffer, 'toJSON', {value: function(){
		var realArray = [];
		for(var i = 0; i < buffer_length; i++)
		{
			realArray[i] = this[i];
		}
		return realArray;
	}});
	
	return buffer;
}


//EXPORTED VARIABLES


//EXPORTED FUNCTIONS

/*
This function returns an extension wrapper for any object.
This wrapper fetches data from the original object when available, and from the object model's default values otherwise.
*/
function getExtensionWrapperFor(value, objectModel)
{
	if(Array.isArray(value))
	{
		// First and only row of the model table is the row model.
		var rowModel = objectModel ? objectModel[0] : null;
		
		return getArrayExtensionWrapper(
			value,
			rowModel
		);
	}
	else if((value != null) && 
		(typeof value == 'object'))
	{
		// If property is a non null object, generate and return an extension wrapper for it.
		return getObjectExtensionWrapper(
			value,
			objectModel
		);
	}
	else
	{
		// If property is a scalar value, return it directly.
		return value;
	}
}

/*
This function returns a buffer wrapper for an object.
This wrapper saves all changes to the object into a diff element, without altering the original object.

An initial diff can be provided, which contents supersede the original object's.
*/
function getBufferWrapperFor(value, initialDiff)
{
	if(Array.isArray(value))
	{
		if(initialDiff && !initialDiff.originalRowEdits)
		{
			// If there is a diff provided but not of array type, update it.
			initialDiff.originalRowEdits = {}; 
			initialDiff.blockEdits = [];
			initialDiff.length = -1;
		}
		
		return getArrayBufferWrapper(
			value,
			initialDiff
		);
	}
	else if((value != null) && 
		(typeof value == 'object'))
	{
		// If property is a non null object, generate and return a buffer wrapper for it.
		return getObjectBufferWrapper(
			value,
			initialDiff
		);
	}
	else
	{
		// If property is a scalar value, return it directly.
		return value;
	}
}

/*
This function returns a buffer wrapper over an extension wrapper, to cumulate effects of both.
*/
function getPartialObjectBufferWrapper(originalObject, objectModel, initialDiff)
{
	// If objectModel given, use extension wrapper; otherwise it is useless.
	var to_buffer = objectModel ? getExtensionWrapperFor(originalObject, objectModel) : originalObject;
	
	return getBufferWrapperFor(to_buffer, initialDiff);
}

//END OF MODULE
Modules.complete('objects_buffer_wrappers');
